# Stage 1
FROM node:10-alpine as build-step

RUN mkdir -p /app

WORKDIR /app

COPY package.json /app
COPY nginx.conf /app
RUN npm install

COPY . /app
RUN npm install -g @angular/cli
RUN ng build --prod
# Stage 2
FROM arm32v7/nginx
COPY --from=build-step /app/dist/listaspesaapp /usr/share/nginx/html
COPY --from=build-step /app/nginx.conf /etc/nginx/conf.d/default.conf