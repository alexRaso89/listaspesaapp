import { TestBed } from '@angular/core/testing';

import { ApiSpesaService } from './api-spesa.service';

describe('ApiSpesaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiSpesaService = TestBed.get(ApiSpesaService);
    expect(service).toBeTruthy();
  });
});
