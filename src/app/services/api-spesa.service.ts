import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiSpesaService {
  listaSpesa=[];
  listaProponi;
  url=environment.api_url;
  constructor(private http:HttpClient) { }

  async getList(){
    var dataSpesa;
    dataSpesa=await this.http.get(this.url+"/listaSpesa").toPromise();
    console.log(dataSpesa.data[0].contenuto);
    this.listaSpesa=dataSpesa.data[0].contenuto;
    
    return this.listaSpesa;
  }
   getList1(){
    var dataSpesa;
    this.http.get(this.url+"/listaSpesa").subscribe(dataLista =>{
      console.log(dataLista["data"][0].contenuto);
      this.listaSpesa=dataLista["data"][0].contenuto;
      console.log(this.listaSpesa);
      return this.listaSpesa;
  });
 
  }
  setList(lista){

    console.log("da inviare",lista);
    this.http.post<any>(this.url+"/listaSpesa", lista).subscribe(data => {
     
  });
  }
  getlistaSpesa(){
    return this.listaSpesa
  }
  setListaSpesa(lista){
    this.listaSpesa=lista;
    this.listaSpesa.sort(this.compare);
  }
  setPreferred(listPreferred){
    console.log("apiservices",listPreferred);
    listPreferred=["Parmigiano"];
    this.http.post<any>(this.url+'/preferiti', listPreferred).subscribe(data => {
     
    });
  }
  async getPreferred(){
    var preferiti;
    preferiti=await this.http.get(this.url+"/preferiti").toPromise();
    console.log(preferiti[0].contenuto);
    this.listaProponi=preferiti[0].contenuto;
    return preferiti[0].contenuto;
  }
  getListaProponi(){
    return this.listaProponi;
  }
  compare( a, b ) {
    if ( a.check && !b.check ){
      return 1;
    }
    if ( !a.check && b.check){
      return -1;
    }
    return 0;
  }
}

