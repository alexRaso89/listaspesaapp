import { Component, ElementRef, HostListener, OnInit, ViewChild  } from '@angular/core';
import { MatAutocomplete, MatBottomSheetRef,MatAutocompleteSelectedEvent } from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { FormControl,FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { variable } from '@angular/compiler/src/output/output_ast';
import { ApiSpesaService } from '../services/api-spesa.service';
import { Router } from '@angular/router';
export interface Fruit {
  name: string;
}
@Component({
  selector: 'app-addin',
  templateUrl: './addin.component.html',
  styleUrls: ['./addin.component.css']
})


export class AddinComponent implements OnInit {
  
  
  value = 'Clear me';
  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  listCtrl = new FormControl();
  itemSelectedControl=new FormControl();
  filteredProponi: Observable<string[]>;
  daComprare: string[] = [];
  listaProponi: string[] = [];
  @ViewChild('articoloInput',{static: false}) articoloInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto',{static: false}) matAutocomplete: MatAutocomplete;
  preferiti: {};
  /*constructor(private _bottomSheetRef: MatBottomSheetRef<AddinComponent>,private apiSpesa:ApiSpesaService) { 

    this.filteredProponi = this.listCtrl.valueChanges.pipe(startWith(null),map((scritto: string | null) => scritto ? this._filter(scritto) : this.listaProponi.slice()));
  }*/
  constructor(private apiSpesa:ApiSpesaService,private router:Router) { 

    this.filteredProponi = this.listCtrl.valueChanges.pipe(startWith(null),map((scritto: string | null) => scritto ? this._filter(scritto) : this.listaProponi.slice()));
  }
  @HostListener('window:beforeunload') goToPage() {
  
    this.router.navigate(['']);
    
  }
  ngOnInit() {
    //this.getPreferred();
    console.log(this.apiSpesa.getListaProponi());
    this.listaProponi=this.apiSpesa.getListaProponi().map(function(el){
      return el.name;
    });
    console.log(this.listaProponi);
  }
  openLink(event: MouseEvent): void {
    //this._bottomSheetRef.dismiss();
    //event.preventDefault();
  }

 
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.daComprare.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.listCtrl.setValue(null);
  }

  remove(el: string): void {
    const index = this.daComprare.indexOf(el);

    if (index >= 0) {
      this.daComprare.splice(index, 1);
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.daComprare.push(event.option.viewValue);
    this.articoloInput.nativeElement.value = '';
    this.listCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.listaProponi.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }
  salvaLista(daInviare){
    console.log("daINviare",daInviare._value);
   var daInviare=this.itemSelectedControl.value;
   console.log("itemSpesa",this.itemSelectedControl)
    var grupDaInviare=this.apiSpesa.getlistaSpesa();
    daInviare.forEach(element => {
      var trovato=0;
      if(grupDaInviare!=undefined){
      for (let i = 0; i < grupDaInviare.length; i++) {
        if (element.toLowerCase()==grupDaInviare[i].name) {
          grupDaInviare[i].quantity++;
          trovato=1;
        }
        
      }
    }
      if (trovato==0) {
        grupDaInviare.push({"id":1,"name":element.toLowerCase(),"quantity":1,"check":false,"timestamp":Date.now()});
      }
    });
    console.log(grupDaInviare);
    this.apiSpesa.setList(grupDaInviare);
    this.apiSpesa.setListaSpesa(grupDaInviare);
    console.log("addinComponent:da inviare per preferiti",daInviare);
    this.apiSpesa.setPreferred(daInviare);
    //{"contenuto":{"items":[{"id":"1","name":"pane","quantity":"1"}]}}
    this.gotoDashboard();  
  
  }
  gotoDashboard(){
    this.router.navigate(['/']);
  }
  /*async getPreferred(){
    
    this.listaProponi= await this.apiSpesa.getPreferred().then(res=>{
      var a=[];
      a.push(res.map(function(el){
        return el.name;
      }));
      return a;
    });
    
  }*/
selectionChange(option:any)
{
  console.log(option);
  let value=this.itemSelectedControl.value || []
  if (option.selected)
    value.push(option.value)
  else
    value=value.filter((x:any)=>x!=option.value)
  this.itemSelectedControl.setValue(value)
}

}
