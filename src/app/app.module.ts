import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArraySortPipe, DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule, MatBadge, MatBadgeModule, MatBottomSheet, MatBottomSheetModule, MatButton, MatButtonModule, MatChipsModule, MatDrawer, MatDrawerContainer, MatDrawerContent, MatFormField, MatFormFieldModule, MatGridList, MatGridListModule, MatIcon, MatIconModule, MatInputModule, MatLabel, MatListModule, MatSelect, MatSelectionList, MatSelectModule, MatSidenavModule, MatSliderModule, MatTabNav, MatTabsModule, MatToolbarModule } from '@angular/material';
import { AddinComponent } from './addin/addin.component';
import { ApiSpesaService } from './services/api-spesa.service';
import { HttpClientModule } from '@angular/common/http';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AddinComponent,
    ArraySortPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    MatSelectModule,
    MatSliderModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTabsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatBadgeModule,
    MatGridListModule,
    MatBottomSheetModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatButtonModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })

    
  ],
  providers: [ApiSpesaService],
  exports:[
    ArraySortPipe
  ],
  entryComponents:[AddinComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
