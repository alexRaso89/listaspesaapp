import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddinComponent } from './addin/addin.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  {path:"dashboard", component:DashboardComponent},
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: 'add', component:AddinComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
