import { Component, HostListener, OnInit, Pipe, PipeTransform } from '@angular/core';
import { MatBottomSheet } from '@angular/material';
import { Router } from '@angular/router';
import { AddinComponent } from '../addin/addin.component';
import { ApiSpesaService } from '../services/api-spesa.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  showFiller = false;
  listaSpesa=[];
  listaSpesaCheck=[];
  interval=null;
  @HostListener('window:beforeunload')
  doSomething() {
    if(this.interval !==null){
    window.alert("sto chiudendo");
    this.apiSpesa.setList(this.listaSpesa);
    }
    //
  }
  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    if(this.interval !==null){
      window.alert("sto chiudendo");
      this.apiSpesa.setList(this.listaSpesa);
      }
  }
  constructor(private _bottomSheet: MatBottomSheet, private apiSpesa:ApiSpesaService, private router:Router) { 

  }

  ngOnInit() {
    this.getList();
    this.getPreferred()
    //console.log(this.apiSpesa.getList1());
    //this.listaSpesa=this.apiSpesa.getList1();
    
  }
  openBottomSheet(): void {
    this.router.navigate(['/add'])
    //this._bottomSheet.open(AddinComponent,{ panelClass: 'customSheet'});

  }
  async getList(){
     await this.apiSpesa.getList().then(res=>{
      /*res.forEach(element => {
        if (element.check) {
          this.listaSpesaCheck.push(element)
        }else{
          this.listaSpesa.push(element)
        }
       
      });*/
      this.listaSpesa=res;
      this.listaSpesa.sort(this.compare);
      console.log(this.listaSpesa)
      
      return ;
    });
    
  }
  async getPreferred(){
    await this.apiSpesa.getPreferred().then(res=>{
      
      return 
    });
    
  }
  updateCheck(i){
    this.listaSpesa[i].check=!this.listaSpesa[i].check;
    this.listaSpesa.sort(this.compare);
    console.log(this.listaSpesa,i);
    console.log(this.interval);
    this.startTime();
    
  }
  compare( a, b ) {
    if ( a.check && !b.check ){
      return 1;
    }
    if ( !a.check && b.check){
      return -1;
    }
    return 0;
  }
  identify(index,item){return item.check;}


  startTime() {
   if(this.interval !==null){
    this.stopTime();
   }
   this.stopTime();
    this.interval = setTimeout(() => {
      console.log("timeout");
      this.apiSpesa.setList(this.listaSpesa);
    }, 5000);


  }
  forceTime(){
    if(this.interval !==null){
      this.stopTime();
      //update data
      this.apiSpesa.setList(this.listaSpesa);
    }
  }
  stopTime() {
    clearInterval(this.interval);
    this.interval=null;
  }
  onSwipeRight(i){
    console.log(i);
    this.listaSpesa.splice(i,1);
    this.startTime();
  }


}
@Pipe({
  name: "sort"
})
export class ArraySortPipe  implements PipeTransform {
  transform(array: any): any[] {
    if (!Array.isArray(array)) {
      return;
    }
    array.sort(this.compare);
    return array;
  }
   compare( a, b ) {
    if ( a.check && !b.check ){
      return -1;
    }
    if ( !a.check && b.check){
      return 1;
    }
    return 0;
  }
  
}
